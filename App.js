import {createStackNavigator} from 'react-navigation'
import React,{ Component } from "react";
import {View,Text,Image,ScrollView,Button,TouchableOpacity,Dimensions,TouchableHighlight,FlatList} from 'react-native'

const {width,height} = Dimensions.get("window")
const LogoTitle = () =>(
  <View style={{ justifyContent: "center",width:width,flex:1,flexDirection:"row" }}>
    <Text style={{ color: "white" ,fontSize:20,}}>FactSumo</Text>
  </View>
)

class App extends Component{
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    // headerTintColor: '#2CADE7',
    headerStyle: {
      backgroundColor: '#2CADE7',
    },
    headerRight: (
      <TouchableOpacity>
        <Text style={{color:"white",borderBottomWidth:2,borderBottomColor:"white"}}>Sign Out</Text>
      </TouchableOpacity>
    ),
    headerLeft: (
      <TouchableOpacity>
        <Text style={{color:"white",fontSize:30}}>{"  <"}</Text>
      </TouchableOpacity>
    ),

  };
  render(){
    return(
      <View style={{
        backgroundColor: "#2CADE7",
        width: width,
        height: height
      }}>
        <ScrollView
          style={{
            flex: 1,
            height:height*0.8
          }}
        >


          <View style={{
            marginRight: "auto",
            marginLeft: "auto"
            // flex:1,justifyContent:"center"
          }}>
            <Image source={require('./sumo.png')} style={{
              width: width * 0.3,
              height: width * 0.3
            }} />
          </View>


          <View
            style={{

            }}
          >
            <Text style={{
              marginLeft: "auto",
              marginRight: "auto",
              color: "white",
              fontSize: 24,
              fontFamily: "Comic Sans MS"
            }}>
              Master Sumo
            </Text>
          </View>

          <View
            style={
              {
                backgroundColor: "white",
                height: height * 0.25,
                width: width * 0.88,
                marginLeft: "auto",
                marginRight: "auto",
                borderRadius: 5,
              }
            }
          >

            <FlatList
              keyExtractor={(item, index) => index}
              data={[
                "","","",
                "  * Progress Saved", "",
                "  * Built-in Study Breaks", "",
                "  * New Sessions Every 15 Mins", ""
              ]}
              renderItem={({ item }) =>
                <View>
                  <Text
                    style={{ color: "black", fontSize: 16 }}
                  >{item}</Text>
                </View>
              }
            />

          </View>
          <View style={{ height: height * 0.08 }}></View>
          <View
            style={{
              flex: 1,
              flexDirection: "row"
            }}
          >
            <TouchableHighlight
              style={{
                width: width * 0.44,
                borderRadius: 4,
                marginLeft: width * 0.04,
                marginRight: width * 0.04,
                backgroundColor: "white",
                height: height * 0.1,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  marginLeft: "auto",
                  color: "#2CADE7",
                  marginRight: "auto"
                }}

              >$1.99 per month</Text>
            </TouchableHighlight>
            <TouchableHighlight style={{
              width: width * 0.44,
              justifyContent: "center",
              borderRadius: 4,
              marginRight: width * 0.04,
              backgroundColor: "white",
              height: height * 0.1
            }}>
              <Text
                style={{
                  marginLeft: "auto",
                  color: "#2CADE7",
                  marginRight: "auto"
                }}
              >$19.99 per year</Text>
            </TouchableHighlight>

          </View>

          <View>
            <Text
              style={{
                fontSize: 24,
                color: "white"
              }}
              >
              Purchase Terms
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: "white"
              }}
              
              >
              Subscription will be charged to your credit card through your iTunes account at confimation of purchase
            </Text>
          </View>
              </ScrollView>
      </View>
    )
  }
}

export default createStackNavigator({
  Home:{
    screen:App
  }
},{
})